# USB Devices Dump

## Keebio: cb10

* cb10:2133 `BDN9 Rev. 2`
* cb10:4256 `Iris Rev. 4`

## Logitech, Inc.: 046d

* 046d:c53a `PowerPlay Wireless Charging System`
* 046d:081b Webcam C310
* 046d:c094 PRO X Wireless - PRO X Wireless Superlight
* 046d:0ab7 `Blue Microphones` - Blue Yeti (Classic)
* 046d:c215 `Extreme 3D Pro`

## Micron Technology, Inc.: 0634

* 0634:5602 `Crucial X6 SSD` - 500 GB

## ASUSTek Computer, Inc.: 0b05

* 0b05:18f3 `AURA LED Controller` - Motherboard LED controller

## Cooler Master Co., Ltd.: 2516

* 2516:0051 `AMD SR4 lamplight Control` - Wraith Prism LED

## Kingston Technology: 0951

* 0951:1718 `HyperX Cloud II Wireless` - wireless receiver
* 0951:1719 HyperX Colud II Wireless - wireless headset

## Realtek Semiconductor Corp.: 0bda

* 0bda:8771 `Bluetooth Radio` - USB Bluetooth adapter

## Elan Microelectronics Corp.: 04f3

* 04f3:0c3d `ELAN:Fingerprint` - USB fingerprint reader

## Genesys Logic, Inc.: 05e3

* 05e3:0608 `Hub` - NZXT Internal USB Hub (Gen 3)

## Xiaomi Inc.: 18d1

* 18d1:4ee7 mobile phone - Mi/Redmi series (MTP + ADB)

## XP-Pen: 28bd

* 28bd:080a 11.6 inch PenDisplay

## STMicroelectronics: 0483

* 0483:5750 5 inch monitor - LED badge -- mini LED display -- 11x44

## Super Top: 14cd

* 14cd:1212 `microSD card reader (SY-T18)`
